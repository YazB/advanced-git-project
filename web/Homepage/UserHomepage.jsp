<%--
  Created by IntelliJ IDEA.
  User: sl297
  Date: 17/01/2019
  Time: 3:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>UserHomepage</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <div class="row">

        <div class="col-sm-3">
            <div class="card">
                <img src="img_avatar.png" alt="Avatar" style="width:100%"><br>
                <div class="container">
                    <h6><b>Your are</b></h6>
                    <input type="text" name="username" id="user_name"><br>
                    <h6><b>From</b></h6>
                    <input type="text" name="userlocation" id="user_location"><br>
                    <hr>

                    <input type="submit" value="Explore!" name="explore" id="explore" class="btn btn-primary "><br><br>
                </div>
            </div>
        </div>


        <div class="col-sm-9">
            <div class="container text-center">
                <form>
                    <textarea class="form-control" rows="4" id="grateful"
                              placeholder="What are you grateful for?"></textarea><br>

                    <input type="submit" value="Save this Memory!" name="memory" id="memory"
                           class="btn btn-primary btn-lg"><br>
                </form>


                <form>
                    <textarea class="form-control" rows="4" id="" placeholder="Yesterday you were grateful for:" ></textarea><br>

                    <textarea class="form-control" rows="4" placeholder="On Tuesday you were grateful for:" ></textarea><br>

                    <%--<input type="submit" value="Show me more memories!" name="loadmemories" id="loadmemories"--%>
                           <%--class="btn btn-primary btn-lg"><br>--%>

                    <div id="memories-load-button" class="memories-load button">Show me some more memories!</div>

                </form>


            </div>
        </div>
    </div>
</div>


<div class="container text-center">
    <hr>
    <span>Don't forget your Login Code!</span>
    <span id="logincode">123</span>
    <input type="submit" value="Logout" name="logout" id="logout" class="btn btn-primary ">
</div>




</body>
</html>
